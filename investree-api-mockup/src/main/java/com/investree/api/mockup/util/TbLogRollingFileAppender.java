package com.investree.api.mockup.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Priority;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.helpers.CountingQuietWriter;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggingEvent;


public class TbLogRollingFileAppender  extends RollingFileAppender{

	private String					_dateFormat = "ddMMyyyy_HHmmss_SSS";
	private SimpleDateFormat		_dateFormatter = new SimpleDateFormat(_dateFormat);
	private boolean					_noLevel = true;
	private Level					_minLevel;
	private Level					_maxLevel;
	private Thread					_cleanupThread;

	/**
	 * The default constructor simply calls its {@link
	 * FileAppender#FileAppender parents constructor}.  */
	public TbLogRollingFileAppender()
	{
		super();
		setMaxBackupIndex(0);
	}
	
	/**
	 * Instantiate a bnsFileAppender and open the file designated by
	 * <code>filename</code>. The opened filename will become the ouput
	 * destination for this appender.
	 *
	 * <p>If the <code>append</code> parameter is true, the file will be
	 * appended to. Otherwise, the file desginated by
	 * <code>filename</code> will be truncated before being opened.
	 */
	public TbLogRollingFileAppender(Layout layout, String filename, boolean append)
	throws IOException
	{
		super(layout, filename, append);
		setMaxBackupIndex(0);
	}
	
	/**
	 * Instantiate a FileAppender and open the file designated by
	 * <code>filename</code>. The opened filename will become the output
	 * destination for this appender.
	 *
	 * <p>The file will be appended to.  */
	public TbLogRollingFileAppender(Layout layout, String filename) throws IOException
	{
		super(layout, filename);
		setMaxBackupIndex(0);
	}

	/** The overriden method is synchronized and checks for the threshold in synchronized
	 * mode by calling isAsSevereAsThreshold. This override is not synchronized and
	 * as the threshold check need not be synchronized. It also compares event level with
	 * configured minLevel or maxLevel.
	 */
	@Override
	public void append(LoggingEvent event)
	{
		if (_noLevel)
		{
			if (super.isAsSevereAsThreshold(event.getLevel()))
				super.append(event);
		}
		else
		{
			int levelInt = event.getLevel().toInt();
			if (_minLevel != null)
			{
				if (levelInt >= _minLevel.toInt())
				{
					if (_maxLevel == null
						|| levelInt <= _maxLevel.toInt())
						super.append(event);
				}
			}
			else if (levelInt <= _maxLevel.toInt())
				super.append(event);
		}
	}

	/** This method is called by super.append to check for the threshold. The overridden
	 * append method checks the threshold before it calls super.append, so by the time
	 * this method is called the threshold has already been checked.
	 */
	@Override
	public boolean isAsSevereAsThreshold(Priority priority)
	{
		return true;
	}

	/**
	 * Implements the usual roll over behaviour.
	 *
	 * <p>If <code>MaxBackupIndex</code> is > 0, then files older files are
	 * deleted to keep <code>MaxBackupIndex</code> number of files.
	 *
	 * <p>If <code>MaxBackupIndex</code> is >=0 then the current file is closed
	 * and renamed to <code>File.formattedDate</code>. A new <code>File</code>
	 * is created to receive further log output.
	 *
	 * <p>If <code>MaxBackupIndex</code> is equal to zero, then the
	 * <code>File</code> is truncated with no backup files created.
	 *
	 */
	@Override
	public synchronized void rollOver() // synchronization not necessary since doAppend is already synched
	{
		LogLog.debug(getName() + ": rolling over count=" + ((CountingQuietWriter) qw).getCount());
		LogLog.debug(getName() + ": maxBackupIndex=" + maxBackupIndex);

		String filePrefix, fileSuffix;

		int pos = fileName.lastIndexOf('.');
		if (pos > 0)
		{
			filePrefix = fileName.substring(0, pos + 1);
			fileSuffix = fileName.substring(pos);
		}
		else
		{
			filePrefix = fileName;
			fileSuffix = "";
		}

		File file = new File(fileName);

		// If maxBackupIndex >= 0, then there current file wil be renamed.
		if (maxBackupIndex >= 0)
		{
			closeFile();
			renameFile(file, filePrefix, fileSuffix);
		}
		
		// If maxBackups > 0, then old files will be deleted.
		if (maxBackupIndex > 0)
			startCleanupThread(file, filePrefix, fileSuffix);

		try
		{
			// This will also close the file. This is OK since multiple
			// close operations are safe.
			setFile(fileName, false, bufferedIO, bufferSize);
		}
		catch(IOException e)
		{
			LogLog.error(getName() + ": setFile("+fileName+", false) call failed.", e);
		}
	}
	
	public synchronized String getDateFormat()
	{
		return _dateFormat;
	}

	/**
	 *  The default date format is "yyyyMMdd_HHmmss_SSS".
	 */
	public synchronized void setDateFormat(String dateFormat)
	{
		_dateFormatter = new SimpleDateFormat(dateFormat);
		_dateFormat = dateFormat;
	}

	public void setMinLevel(String levelStr)
	{
		_minLevel = Level.toLevel(levelStr);
		setNoLevel();
	}

	public void setMaxLevel(String levelStr)
	{
		_maxLevel = Level.toLevel(levelStr);
		setNoLevel();
	}

	private void setNoLevel()
	{
		_noLevel = (_minLevel == null && _maxLevel == null);
	}

	private void renameFile(File file, String filePrefix, String fileSuffix)
	{
		File target = new File(filePrefix + _dateFormatter.format(new Date()) + fileSuffix);
		LogLog.debug(getName() + ": Renaming file " + file.getAbsolutePath() + " to " + target.getAbsolutePath());

		if (file.renameTo(target))
			return;

		// If rename fails, then copy the current file to the target name
		LogLog.error(getName() + ": Renaming file " + file.getAbsolutePath() + " to " + target.getAbsolutePath() + " failed");
		FileChannel copyFrom = null;
		FileChannel copyTo = null;

		try
		{
			copyFrom = new FileInputStream(file).getChannel();
			copyTo = new FileOutputStream(target).getChannel();
			LogLog.debug(getName() + ": Copying file " + file.getAbsolutePath() + " to " + target.getAbsolutePath());
			copyFrom.transferTo(0, copyFrom.size(), copyTo);

			if (target.setLastModified(file.lastModified()))
				LogLog.warn(getName() + ": Cannot set last modified time from file " + file.getAbsolutePath() + " to " + target.getAbsolutePath());
		}
		catch (Exception ex)
		{
			LogLog.error(getName() + ": Cannot copy file " + file.getAbsolutePath() + " to " + target.getAbsolutePath(), ex);
		}
		finally
		{
			try
			{
				if (copyFrom != null )
					copyFrom.close();
				if (copyTo != null)
					copyTo.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}
	}

	private void startCleanupThread(final File file, String pathPrefix, final String fileSuffix)
	{
		if (maxBackupIndex <= 0
			|| _cleanupThread != null)
		{
			LogLog.debug(getName() + ": Cleanup thread already runing for " + file.getAbsolutePath());
			return;
		}

		final String appenderName = getName();
		final String filePrefix = new File(pathPrefix).getName(); // this is to strip any dir name

		_cleanupThread = new Thread()
		{
			private int length = file.getName().length(); // this is to strip any dir name
			@Override
			public void run()
			{
				long start = System.currentTimeMillis();
				try
				{
					LogLog.debug(appenderName + ": Started cleanup thread for " + file.getAbsolutePath());
					File parentFolder = file.getParentFile();
					if (parentFolder == null)
						parentFolder = new File(".");

					// Get all the files with the filename
					File files[] = parentFolder.listFiles(
										new FilenameFilter()
										{
											public boolean accept(File dir, String name)
											{
												boolean isLog = name.length() > length && name.startsWith(filePrefix) && name.endsWith(fileSuffix);
												return isLog;
											}
										});

					if (files.length <= maxBackupIndex)
						return;

					FileWithModTime filesWithModTime[] = new FileWithModTime[files.length];
					for (int i = 0; i < filesWithModTime.length; i++)
						filesWithModTime[i] = new FileWithModTime(files[i]);

					files = null;

					// Sort the files to get the oldest files first
					Arrays.sort(
						filesWithModTime,
						new Comparator<FileWithModTime>()
						{
							public int compare(FileWithModTime f1, FileWithModTime f2)
							{
								if (f1._lastModified < f2._lastModified)
									return -1;
								if (f1._lastModified == f2._lastModified)
									return 0;
								return 1;
							}
						});

					int numFilesToDelete = filesWithModTime.length - maxBackupIndex;
					LogLog.debug(appenderName + ": Will attempt to delete " + numFilesToDelete + " file(s)");
					for (int i = 0; i < numFilesToDelete; i++)
					{
						LogLog.debug(appenderName + ": Deleting file " + filesWithModTime[i]._file.getAbsolutePath());
						if (!filesWithModTime[i]._file.delete())
							LogLog.error(appenderName + ": Cannot delete file " + filesWithModTime[i]._file.getName());
					}
				}
				catch (Throwable ex)
				{
					LogLog.error(appenderName + ": Cannot delete files", ex);
				}
				finally
				{
					LogLog.debug(appenderName + ": Ended cleanup thread for " + file.getAbsolutePath() + "  DIFF " + (System.currentTimeMillis() - start) + "ms");
					_cleanupThread = null;
				}
			}
		};

		_cleanupThread.start();
	}

	private static class FileWithModTime
	{
		private File				_file;
		private long				_lastModified;
		private FileWithModTime(File file)
		{
			_file = file;
			_lastModified = file.lastModified();
		}
	}

}
