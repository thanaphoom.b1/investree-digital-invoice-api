package com.investree.api.mockup.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;
import com.investree.api.mockup.domain.entity.invoce.InvoiceRqPostPayload;
import com.investree.api.mockup.domain.entity.invocebatch.InvoiceBatchRqtPayload;
import com.investree.api.mockup.domain.entity.invoiceStatus.InvoiceStatusRqPayload;
import com.investree.api.mockup.domain.entity.invoiceadjust.InvoiceAdjustRqPayload;
import com.investree.api.mockup.services.MockUpInvoiceService;

@EnableWebMvc
@RestController
@CrossOrigin
@RequestMapping("/")
public class MockUpInvoiceController {

	private static final Logger log = Logger.getLogger(MockUpInvoiceController.class);

	Gson gson = new Gson();
	MockUpInvoiceService service = new MockUpInvoiceService();

	
	@GetMapping(path = "/check")
	public String check(@RequestParam String invoiceID){
		
		String response = invoiceID;
		log.info(response);
		
		return response;
	}
	
	@RequestMapping(value = "invoice", method = RequestMethod.POST)
	public Object postInvoice(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestBody InvoiceRqPostPayload request) {

		log.info("[Post API invoice]");
		log.info("[Post API invoice] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);

		log.info("[Post API invoice] -Request Data :" + gson.toJson(request));

		return service.postInvoiceService(request);
	}

	@RequestMapping(value = "invoice", method = RequestMethod.GET)
	public Object getInvoice(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestParam("invoiceID") String invoiceID,
			@RequestParam("sellerID") String sellerID, @RequestParam("buyerID") String buyerID,
			@RequestParam("issueDate") String issueDate) {

		log.info("[Get API invoice]");
		log.info("[Get API invoice] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Get API invoice] - Request Params? invoiceID =" + invoiceID + ", sellerID=" + sellerID + ", buyerID="
				+ buyerID + ", issueDate=" + issueDate);

		return service.getInvoiceService(invoiceID, sellerID, buyerID, issueDate);

	}

	@RequestMapping(value = "invoiceBatch", method = RequestMethod.POST)
	public Object postinvoiceBatch(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestBody InvoiceBatchRqtPayload request) {

		log.info("[Post API invoiceBatch]");
		log.info("[Post API invoiceBatch] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Post API invoiceBatch] - Request Data :" + gson.toJson(request));

		return service.postinvoiceBatchServices(request);
	}

	@RequestMapping(value = "invoiceStatus", method = RequestMethod.GET)
	public Object getInvoiceStatus(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestParam("status") String status,
			@RequestParam("invoiceID") String invoiceID, @RequestParam("sellerID") String sellerID,
			@RequestParam("buyerID") String buyerID, @RequestParam("issueDate") String issueDate) {

		log.info("[Get API invoiceStatus]");
		log.info("[Get API invoiceStatus] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Get API invoiceStatus] - Request Params? status =" + status + ", invoiceID =" + invoiceID
				+ ", sellerID=" + sellerID + ", buyerID=" + buyerID + ", issueDate=" + issueDate);

		return service.getInvoiceStatusService(status, invoiceID, sellerID, buyerID, issueDate);

	}

	@RequestMapping(value = "invoiceStatus", method = RequestMethod.POST)
	public Object postInvoiceStatus(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestBody InvoiceStatusRqPayload request) {

		log.info("[Post API invoiceStatus]");
		log.info("[Post API invoiceStatus] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Post API invoiceStatus] - Request Data :" + gson.toJson(request));

		return service.postInvoiceStatusService(request);

	}

	@RequestMapping(value = "invoiceAdjust", method = RequestMethod.POST)
	public Object postInvoiceAdjust(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestBody InvoiceAdjustRqPayload request) {

		log.info("[Post API invoiceAdjust]");
		log.info("[Post API invoiceAdjust] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Post API invoiceAdjust] - Request Data :" + gson.toJson(request));

		return service.postInvoiceAdjustService(request);

	}

	@RequestMapping(value = "sellerDoubleFinance", method = RequestMethod.GET)
	public Object getSellerDoubleFinance(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestParam(value="sellerID") String sellerID) {

		log.info("[Get API sellerDoubleFinance]");
		log.info("[Get API sellerDoubleFinance] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Get API sellerDoubleFinance] - Request Params? sellerID=" + sellerID);

		return service.getSellerDoubleFinanceService(sellerID);

	}

	@RequestMapping(value = "contract", method = RequestMethod.GET)
	public Object getContract(@RequestHeader("Content-Type") String contentType,
			@RequestHeader("X-IBM-Client-Id") String clientId,
			@RequestHeader("X-IBM-Client-Secret") String clientSecret, @RequestParam("sellerID") String sellerID,
			@RequestParam("buyerID") String buyerID, @RequestParam("poRef") String poRef) {

		log.info("[Get API contract]");
		log.info("[Get API contract] - Content-Type =" + contentType + ", X-IBM-Client-Id =" + clientId
				+ ", X-IBM-Client-Secret =" + clientSecret);
		log.info("[Get API contract] - Request Params? sellerID=" + sellerID);

		return service.getContractService(sellerID, buyerID, poRef);
	}

}
