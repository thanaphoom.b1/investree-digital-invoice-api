package com.investree.api.mockup.lambda;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.serverless.exceptions.ContainerInitializationException;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.serverless.proxy.spring.SpringBootLambdaContainerHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.investree.api.mockup.InvestreeApiMockupApplication;

public class AWSLambdaHandler implements RequestStreamHandler {

	private static Logger logger = LoggerFactory.getLogger(AWSLambdaHandler.class);

	public static final SpringBootLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse> handler;

	static {
		try {
			handler = SpringBootLambdaContainerHandler.getAwsProxyHandler(InvestreeApiMockupApplication.class);
		} catch (ContainerInitializationException e) {
			String errMsg = "Could not initialize Spring Boot application";
			logger.error(errMsg);
			throw new RuntimeException(errMsg, e);
		}
	}

	@Override
	public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
		handler.proxyStream(inputStream, outputStream, context);
		outputStream.close();
	}

}
