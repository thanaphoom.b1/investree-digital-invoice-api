package com.investree.api.mockup.domain.entity.invoce;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceRsGetPayload {
	
	private String id;
	private String invoiceID;
	private String sellerID;
	private String sellerBranch;
	private String buyerID;
	private String buyerBranch;
	private String platformID; 
	private String poRef;	 
	private Double amount;
	private String dueDate;
	private String issueDate;
	private int status;
	
	private String refHash;  
	private String updateDate;
	private String noteID;
	private String oldAmount;
	private String newAmount;
	private String newDueDate;
	private String desc;	
}