package com.investree.api.mockup.util;

public class Constant {
	
	public static final String C_CONTEXTROOT = "MockUpInvoiceAPIs";
	public static final String C_LOG4J_CONFIG = "log4j.properties";
	
	public static final String SUCCESS = ":success";
		
	public static final String  ERROR_INVALID_INPUTS = "Invalid inputs"; //Missing a parameter
	public static final String  ERROR_INVOICE_ALREADY_EXPIRED = "Invoice already expired"; //Due date has already been passed
	public static final String  ERROR_WARNING_INVOICE_ALREADY_EXISTS = "Warning: Invoice already exists"; //Found an invoice with the same hash in the database
	public static final String  ERROR_PLATFORM_NOT_FOUND = "Platform not found"; //The requested platform is not found in the database
	public static final String  ERROR_INVOICE_NOT_FOUND = "Invoice not found"; //The requested invoice is not found
	public static final String  ERROR_UNAUTHORIZED = "Unauthorized"; //The requested platform is not authorized for this invoice
	
	
	public static final String  ERROR_INVALID_INPUT_STATUS = "Invalid input status"; //Invalid input status code
	public static final String  ERROR_UNABLE_TO_REVERT_INVOICE_STATE = "Unable to revert invoice state"; //Cannot update invoice to a previous state
	public static final String  ERROR_INCORRECT_STATUS_UPDATE_STAGE1 = "Incorrect status update stage 1"; //The input status is not applicable for the invoice status 0
	public static final String  ERROR_INCORRECT_STATUS_UPDATE_STAGE2 = "Incorrect status update stage 2"; //The input status is not applicable for the invoice status 1
	
	public static final String ERROR_OLD_AMOUNT_NOT_MATCH = "Old amount not match with exising invoice or update note"; //The old amount sent is not correct
	
	public static final String ERROR_Seller_not_found = "Seller not found"; //The requested seller is not found in the database
	
	public static final String ERROR_INVALID_REQUEST  = "Invalid request"; ////Missing a parameter

}

