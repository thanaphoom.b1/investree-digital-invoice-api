package com.investree.api.mockup;


import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @author Thanaphoom B.
 * @since 2021-05-25
 */

@SpringBootApplication
@ComponentScan(basePackages = "com.investree.api.mockup")
public class InvestreeApiMockupApplication  extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(InvestreeApiMockupApplication.class, args);
	}
	
	  private final Logger log = LoggerFactory.getLogger(InvestreeApiMockupApplication.class);

	  @SuppressWarnings("unused")
	  @Autowired
	  private Environment env;



	  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(InvestreeApiMockupApplication.class);
	  }
	  
	  @Bean
	    public HandlerMapping handlerMapping() {
	        return new RequestMappingHandlerMapping();
	    }

	    @Bean
	    public HandlerAdapter handlerAdapter() {
	        return new RequestMappingHandlerAdapter();
	    }

	  
	  /**
	   * CommandLineRunner method. Process when initial Spring Boot
	   */
	  @SuppressWarnings("unused")
	  @Bean
	  public CommandLineRunner intro() {
	    return (args) -> {
	      log.info(String.format("Version: %s", env.getProperty("git.build.version")));
	      log.info(String.format("Env: %s!!", env.getProperty("env.key")));
	      log.info(String.format("Server port(property): %s", env.getProperty("server.port")));
	      log.info(
	          String.format("Active profile property: %s", env.getProperty("spring.profile.active")));
	      log.info(String.format("Set override config file: %s",
	          env.getProperty("mt.arch.override.config")));
	      log.info(String.format("Default date timezone: %s", DateTimeZone.getDefault().getID()));
	    };
	  }

}
