package com.investree.api.mockup.domain.entity.invoiceadjust;
import com.investree.api.mockup.domain.entity.common.TransDetail;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceAdjustRqPayload {
		
	private String invoiceID;
	private String sellerID;
	private String buyerID;
	private String issueDate;
	private double oldAmount;
	private double newAmount;
	private String dueDate;
	private String desc;
	private TransDetail transDetail;
}
