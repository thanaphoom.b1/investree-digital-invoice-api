package com.investree.api.mockup.domain.entity.invoiceStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class InvoiceStatusRSPayload {
	private String res;
}
