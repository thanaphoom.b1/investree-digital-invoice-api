package com.investree.api.mockup.domain.entity.invocebatch;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class invoiceBatchRsPostPayload {	
	private List<String> res; 
}
