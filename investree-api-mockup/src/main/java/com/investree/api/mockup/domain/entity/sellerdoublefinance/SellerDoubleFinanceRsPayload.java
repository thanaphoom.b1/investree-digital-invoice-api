package com.investree.api.mockup.domain.entity.sellerdoublefinance;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellerDoubleFinanceRsPayload {
	
	private String sellerID;
	private BigDecimal doubleFinancing;
}
