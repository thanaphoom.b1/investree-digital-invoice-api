package com.investree.api.mockup.domain.entity.contract;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContractRsGetPayload {
	private boolean found;

}
