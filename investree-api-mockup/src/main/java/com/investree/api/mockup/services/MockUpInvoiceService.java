package com.investree.api.mockup.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.investree.api.mockup.domain.entity.common.ErrorResponse;
import com.investree.api.mockup.domain.entity.contract.ContractRsGetPayload;
import com.investree.api.mockup.domain.entity.invoce.InvoiceRqPostPayload;
import com.investree.api.mockup.domain.entity.invoce.InvoiceRsGetPayload;
import com.investree.api.mockup.domain.entity.invoce.InvoiceRsPostPayload;
import com.investree.api.mockup.domain.entity.invocebatch.InvoiceBatchRq;
import com.investree.api.mockup.domain.entity.invocebatch.InvoiceBatchRqtPayload;
import com.investree.api.mockup.domain.entity.invocebatch.invoiceBatchRsPostPayload;
import com.investree.api.mockup.domain.entity.invoiceStatus.InvoiceStatusRSPayload;
import com.investree.api.mockup.domain.entity.invoiceStatus.InvoiceStatusRqPayload;
import com.investree.api.mockup.domain.entity.invoiceadjust.InvoiceAdjustRqPayload;
import com.investree.api.mockup.domain.entity.invoiceadjust.InvoiceAdjustRsPayload;
import com.investree.api.mockup.domain.entity.sellerdoublefinance.SellerDoubleFinanceRsPayload;
import com.investree.api.mockup.util.Constant;

public class MockUpInvoiceService {

	private static final Logger log = Logger.getLogger(MockUpInvoiceService.class);

	ErrorResponse errorResponse;

	public Object postInvoiceService(InvoiceRqPostPayload request) {
		Object response = null;
		try {

			if (request.getInvoiceID().equals("inv_test_000") || request.getInvoiceID().equals("inv_test_004")) {
				errorResponse = new ErrorResponse();
				errorResponse.setError((Constant.ERROR_WARNING_INVOICE_ALREADY_EXISTS));
				response = errorResponse;

			} else if (request.getInvoiceID().equals("inv_test_009") && request.getSellerID().equals("0125562003300")) {
				errorResponse = new ErrorResponse();
				errorResponse.setError((Constant.ERROR_INVOICE_ALREADY_EXPIRED));
				response = errorResponse;
			} else if (request.getInvoiceID().equals("inv_test_010") && request.getAmount() == 0.00) {
				errorResponse = new ErrorResponse();
				errorResponse.setError((Constant.ERROR_INVALID_INPUTS));
				response = errorResponse;
			} else {
				InvoiceRsPostPayload rsOjb = new InvoiceRsPostPayload();
				rsOjb.setRes(request.getSellerID().concat(Constant.SUCCESS));
				response = rsOjb;
			}

		} catch (Exception e) {
			log.info("[Get API invoice] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object getInvoiceService(String invoiceID, String sellerID, String buyerID, String issueDate) {
		Object response = null;
		try {
			if (invoiceID != null && sellerID != null && buyerID != null && issueDate != null) {
				InvoiceRsGetPayload rsOjb = new InvoiceRsGetPayload();

				log.debug("invoiceID = " + invoiceID);
				log.debug("sellerID = " + sellerID);
				log.debug("buyerID = " + buyerID);
				log.debug("issueDate = " + issueDate);

				System.out.println("invoiceID = " + invoiceID);
				System.out.println("sellerID = " + sellerID);
				System.out.println("buyerID = " + buyerID);
				System.out.println("issueDate = " + issueDate);

				if (invoiceID.equals("inv_test_999") && sellerID.equals("0125562003316")
						&& buyerID.equals("0105561130284") && issueDate.equals("2021-05-17")) {
					log.debug("Mockup case 1");
					System.out.println("Mockup case 1");
					errorResponse = new ErrorResponse();
					errorResponse.setError(Constant.ERROR_INVOICE_NOT_FOUND);
					response = errorResponse;
				} else if (invoiceID.equals("inv_test_999") && sellerID.equals("0125562003316")
						&& buyerID.equals("0105561130284") && issueDate.equals("0000-00-00")) {
					log.debug("Mockup case 2");
					System.out.println("Mockup case 2");
					errorResponse = new ErrorResponse();
					errorResponse.setError(Constant.ERROR_INVALID_INPUTS);
					response = errorResponse;
				} else {
					log.debug("Mockup case 3");
					System.out.println("Mockup case 3");
					rsOjb.setId("28");
					rsOjb.setInvoiceID(invoiceID);
					rsOjb.setSellerID(sellerID);
					rsOjb.setSellerBranch("00145");
					rsOjb.setBuyerID(buyerID);
					rsOjb.setBuyerBranch("00125");
					rsOjb.setPlatformID("3302211113826");
					rsOjb.setPoRef("");
					rsOjb.setAmount(200002.116);
					rsOjb.setDueDate("2021-10-19T17:00:00.000Z");
					rsOjb.setIssueDate(issueDate.concat("T17:00:00.000Z"));
					rsOjb.setStatus(1);
					ObjectMapper mapper = new ObjectMapper();
					mapper.setSerializationInclusion(Include.ALWAYS);
					response = mapper.writeValueAsString(rsOjb);
				}

			} else {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_INVALID_INPUTS);
				response = errorResponse;
			}
		} catch (Exception e) {
			log.info("[Get API invoice] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object postinvoiceBatchServices(InvoiceBatchRqtPayload request) {
		invoiceBatchRsPostPayload rsOjb = new invoiceBatchRsPostPayload();
		List<String> list = new ArrayList<>();
		Object response = null;
		try {
			for (InvoiceBatchRq ojb : request.getData()) {
				list.add(ojb.getInvoiceID().concat(Constant.SUCCESS));
			}
			rsOjb.setRes(list);
			response = rsOjb;
		} catch (Exception e) {
			log.info("[Post API invoiceBatch] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object getInvoiceStatusService(String status, String invoiceID, String sellerID, String buyerID,
			String issueDate) {
		Object response = null;
		try {
			if (status != null && invoiceID != null && sellerID != null && buyerID != null && issueDate != null) {
				int staustInt = Integer.parseInt(status);
				InvoiceStatusRqPayload rsOjb = new InvoiceStatusRqPayload();
				rsOjb.setStatus(staustInt);
				rsOjb.setInvoiceID(invoiceID);
				rsOjb.setSellerID(sellerID);
				rsOjb.setBuyerID(buyerID);
				rsOjb.setIssueDate(issueDate);
				response = rsOjb;
			} else {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_INVALID_INPUTS);
				response = errorResponse;
			}
		} catch (NumberFormatException e) {
			errorResponse = new ErrorResponse();
			errorResponse.setError(Constant.ERROR_INVALID_INPUT_STATUS);
			response = errorResponse;
		} catch (Exception e) {
			log.info("[Get API invoiceStatus] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object postInvoiceStatusService(InvoiceStatusRqPayload request) {
		Object response = null;
		try {
			if (request.getStatus() == 1) {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_INCORRECT_STATUS_UPDATE_STAGE1);
				response = errorResponse;
			} else if (request.getStatus() == 2) {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_INCORRECT_STATUS_UPDATE_STAGE2);
				response = errorResponse;
			} else {
				InvoiceStatusRSPayload rsOjb = new InvoiceStatusRSPayload();
				rsOjb.setRes((Constant.SUCCESS).replace(":", ""));
				response = rsOjb;
			}
		} catch (Exception e) {
			log.info("[Post API invoiceStatus] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object postInvoiceAdjustService(InvoiceAdjustRqPayload request) {
		Object response = null;

		try {
			if (request.getOldAmount() != 0) {
				InvoiceAdjustRsPayload rsOjb = new InvoiceAdjustRsPayload();
				rsOjb.setRes((Constant.SUCCESS).replace(":", ""));
				response = rsOjb;

			} else {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_OLD_AMOUNT_NOT_MATCH);
				response = errorResponse;
			}

		} catch (Exception e) {
			log.info("[Post API invoiceAdjust] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object getSellerDoubleFinanceService(String sellerID) {
		Object response = null;
		try {
			if (sellerID != null) {
				SellerDoubleFinanceRsPayload rsOjb = new SellerDoubleFinanceRsPayload();
				rsOjb.setSellerID(sellerID);
				rsOjb.setDoubleFinancing(new BigDecimal(0));
				response = rsOjb;
			} else {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_INVALID_INPUTS);
				response = errorResponse;
			}
		} catch (Exception e) {
			log.info("[Get API sellerDoubleFinance] - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}

	public Object getContractService(String sellerID, String buyerID, String poRef) {
		Object response = null;
		try {
			if (sellerID != null && buyerID != null && poRef != null) {

				ContractRsGetPayload rsOjb = new ContractRsGetPayload();
				rsOjb.setFound(false);
				response = rsOjb;
			} else {
				errorResponse = new ErrorResponse();
				errorResponse.setError(Constant.ERROR_INVALID_REQUEST);
				response = errorResponse;
			}

		} catch (Exception e) {
			log.info("[Get API contract - Error :" + e.getMessage());
			errorResponse = new ErrorResponse();
			errorResponse.setError(e.getMessage());
			response = errorResponse;
		}
		return response;
	}
}
