package com.investree.api.mockup.domain.entity.invocebatch;


import com.investree.api.mockup.domain.entity.common.TransDetail;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceBatchRq{	
	private String invoiceID;
	private String sellerID;
	private String sellerBranch;
	private String buyerID;	
	private String buyerBranch;
	private String platformID;
	private String poRef;
	private double amount;
	private String dueDate;	
	private String issueDate;
	private int isFinanced;
	private TransDetail transDetail;
}


