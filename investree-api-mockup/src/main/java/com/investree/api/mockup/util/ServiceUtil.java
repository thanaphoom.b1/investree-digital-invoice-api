package com.investree.api.mockup.util;

import java.util.Properties;
import java.util.Set;

public class ServiceUtil {
	
	public static String getFilePath(String fileName){
		String path = "";
		try {
			
			 Properties properties = System.getProperties();
		        Set<Object>  sysPropertiesKeys = properties.keySet();
		        for (Object key : sysPropertiesKeys) {
		            System.out.println(key + " =" + properties.getProperty((String)key)); 
		        }
		        
			String url = System.getProperty("user.dir");
			System.out.println("Working directory : "+url);
			StringBuffer absolutePath = new StringBuffer();
			absolutePath.append(url);
			absolutePath.append("/");
			absolutePath.append("config");
			absolutePath.append("/");
			absolutePath.append(Constant.C_CONTEXTROOT);
			absolutePath.append("/");
			absolutePath.append(fileName);
			path = StringUtil.resolveAsPath(absolutePath.toString());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return path;
	}

}
