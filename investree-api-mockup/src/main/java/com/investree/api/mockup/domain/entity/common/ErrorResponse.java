package com.investree.api.mockup.domain.entity.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {
	private String error;
}
