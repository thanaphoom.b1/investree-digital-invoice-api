package com.investree.api.mockup.util;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;


public class LoggerConfig {
	@Autowired
	ResourceLoader resourceLoader;
	
	private static String curDate = "";
	private static LoggerConfig instance;
	static{
	    SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	    System.setProperty("current.date", dateFormat.format(new Date()));
	}
	private LoggerConfig(){
		try {
			
			Properties props = new Properties();
			
			props.load(new FileInputStream(ServiceUtil.getFilePath(Constant.C_LOG4J_CONFIG)));
			PropertyConfigurator.configure(props);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	static synchronized public void getInstance(){
		 SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		 curDate = dateFormat.format(new Date());
		 String logDate = System.getProperty("current.date");
		 if(!curDate.equals(logDate)){
			 System.setProperty("current.date", curDate);
			 instance = new LoggerConfig();
		 }
		if(instance == null){
			instance = new LoggerConfig();
		}
	}

}
