package com.investree.api.mockup.domain.entity.invoiceadjust;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceAdjustRsPayload {
	private String res;
}
