package com.investree.api.mockup.domain.entity.invoiceStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceStatusRqPayload {
		
	private int status;
	private String invoiceID;
	private String sellerID;
	private String buyerID;
	private String issueDate;



}
