package com.investree.api.mockup.domain.entity.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransDetail {
		
	private String trans_ref;
	private String send_date;
	private String send_time;

}
