package com.investree.digitalfactoring.api.util;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateFactory implements FactoryBean<RestTemplate>, InitializingBean {
 
	@Autowired
	private Environment env;
	@Autowired
	Configuration conf;
	
    private RestTemplate restTemplate;

    @SuppressWarnings("deprecation")
	public RestTemplate getObject()throws Exception {
    	
    	KeyStore clientStore = KeyStore.getInstance("PKCS12");
    	String clientStorePath = "classpath:"+env.getProperty(Const.CLIENT_STORE_FILE);
        clientStore.load(new FileInputStream(ResourceUtils.getFile(clientStorePath)), env.getProperty(Const.CLIENT_STORE_PASS).toCharArray());

        SSLContextBuilder sslContextBuilder = new SSLContextBuilder();
        sslContextBuilder.useProtocol("TLSv1.2");
        sslContextBuilder.loadKeyMaterial(clientStore, env.getProperty(Const.CLIENT_STORE_PASS).toCharArray());
        sslContextBuilder.loadTrustMaterial(new TrustSelfSignedStrategy());

        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContextBuilder.build());
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(sslConnectionSocketFactory)
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        
    	
        return new RestTemplate(requestFactory);
    }
    public Class<RestTemplate> getObjectType() {
        return RestTemplate.class;
    }
    public boolean isSingleton() {
        return true;
    }

    public void afterPropertiesSet() {
        HttpHost host = new HttpHost(conf.getProperty(Const.BOT_API_HOST));
        restTemplate = new RestTemplate(
          new HttpComponentsClientHttpRequestFactoryBasicAuth(host));
    }
    
    public HttpHeaders createHeaders(){
    	List<APIHeader> headers = new ArrayList<APIHeader>();
    	headers.add(new APIHeader("Content-Type","application/json"));
    	headers.add(new APIHeader(Const.API_HEADER.CLIENT_ID_NAME,conf.getProperty(Const.API_HEADER.CLIENT_ID_VALUE)));
    	headers.add(new APIHeader(Const.API_HEADER.CLIENT_SECRET_NAME,conf.getProperty(Const.API_HEADER.CLIENT_SECRET_VALUE)));
    	   return new HttpHeaders() {{
    	         for(APIHeader header : headers){
    	     		set(header.getName(),header.getValue());
    	     	}
    	      }};
    	}
}