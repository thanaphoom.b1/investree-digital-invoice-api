package com.investree.digitalfactoring.api.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.investree.digitalfactoring.api.entity.http.HttpRequestModel;

@Component
public class RestAPICaller {
	private static final Logger log = LoggerFactory.getLogger(RestAPICaller.class);

	@Autowired
	Environment env;
	@Autowired
	RestTemplateFactory rest;

	public Object exchange(HttpRequestModel request){
		log.info("Calling API. . . . .");
		log.info("Endpoint: " + request.getEndpoint());
		ResponseEntity<Object> response = null;
		HttpHeaders header = rest.createHeaders();
		RestTemplate restTemplate = null;
		try {
		restTemplate = rest.getObject();
		restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		HttpEntity<Object> requestEntity = new HttpEntity<>(header);
		
		if (request.getMethod().equals(HttpMethod.GET)) { // GET Method
			Map<String, String> uriVariables = request.getUriVariables();

			UriComponentsBuilder builderComponent = UriComponentsBuilder.fromHttpUrl(request.getEndpoint());
			
			for (Map.Entry<String, String> entry : uriVariables.entrySet()) {
				String key = entry.getKey();
				String val = entry.getValue();
				log.debug("Key-Value: "+key+"-"+val);
				builderComponent.queryParam(key, val);
			}
			
			UriComponents builder = builderComponent.build();
			String urlReq = builder.toString();
			log.info("Request URL: "+urlReq);
			response = restTemplate.exchange(urlReq, request.getMethod(), requestEntity, Object.class);
			
		} else { // POST Method
			String requestJson = StringUtil.getJSONStr(request.getRequestpayload());
			log.info("Request JSON: " + requestJson);
			response = restTemplate.exchange(request.getEndpoint(), request.getMethod(),
					new HttpEntity<String>(requestJson, header), Object.class);
		}
		}catch(HttpServerErrorException httpEx) {
			log.debug(httpEx.getMessage());
			log.debug(httpEx.getResponseBodyAsString());
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		log.info("Response Status: " + response.getStatusCode());
		log.info("Response Header: " + response.getHeaders());
		log.info("Response Body: " + response.getBody());

		return response;

	}
}
