package com.investree.digitalfactoring.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.investree.digitalfactoring.api.entity.http.HttpRequestModel;
import com.investree.digitalfactoring.api.entity.invoice.InvoiceRequestPayload;
import com.investree.digitalfactoring.api.service.InvoiceService;
import com.investree.digitalfactoring.api.util.Const;
import com.investree.digitalfactoring.api.util.RestAPICaller;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	RestAPICaller api;
	
	@Autowired
	Environment env;

	@Override
	public Object getInvoice(String invoiceID
			, String sellerID
			, String buyerID
			, String issueDate) {
		
		HttpRequestModel request = new HttpRequestModel();
		request.setEndpoint(env.getProperty(Const.BOT_API_INVOICE));
		request.setMethod(HttpMethod.GET);
		request.setUriVariables("invoiceID", invoiceID);
		request.setUriVariables("sellerID", sellerID);
		request.setUriVariables("buyerID", buyerID);
		request.setUriVariables("issueDate", issueDate);
		
		return api.exchange(request);

	}

	@Override
	public Object setInvoice(Object payload) {
		HttpRequestModel request = new HttpRequestModel();
		request.setEndpoint(env.getProperty(Const.BOT_API_INVOICE));
		request.setMethod(HttpMethod.POST);
		request.setRequestpayload(payload);
		return api.exchange(request);
	}

}
