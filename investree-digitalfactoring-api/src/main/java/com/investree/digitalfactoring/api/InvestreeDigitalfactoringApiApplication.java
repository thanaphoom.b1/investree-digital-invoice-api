package com.investree.digitalfactoring.api;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@SpringBootApplication
@ComponentScan(basePackages = "com.investree.digitalfactoring.api")
@EntityScan("com.investree.digitalfactoring.api.entity")
public class InvestreeDigitalfactoringApiApplication extends SpringBootServletInitializer {

	@Autowired
	private Environment env;
	private static final Logger log = LoggerFactory.getLogger(InvestreeDigitalfactoringApiApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(InvestreeDigitalfactoringApiApplication.class);
	}

	@Bean
	public HandlerMapping handlerMapping() {
		return new RequestMappingHandlerMapping();
	}

	@Bean
	public HandlerAdapter handlerAdapter() {
		return new RequestMappingHandlerAdapter();
	}

	// @Bean
	// protected CommandLineRunner initial() {
	// return args -> {
	// log.info("API: " + env.getProperty("app.name"));
	// // log.info("Post: " + env.getProperty("server.port"));
	// log.info("RUNTIME: " + (System.getenv("RUNTIME") == null ? "localhost
	// runtime" : System.getenv("RUNTIME")));
	// log.info("BOT API HOST: " + (System.getenv("BOT_API_HOST") == null ?
	// env.getProperty(Const.BOT_API_HOST)
	// : System.getenv("BOT_API_HOST")));

	// Map<String, String> mapEnv = System.getenv();
	// log.debug("============== System Environment ============");
	// for (Map.Entry<String, String> entry : mapEnv.entrySet()) {
	// log.debug(entry.getKey() + ":" + entry.getValue());
	// }

	// };
	// }

	public static void main(String[] args) {
		SpringApplication.run(InvestreeDigitalfactoringApiApplication.class, args);
	}

}
