package com.investree.digitalfactoring.api.entity.http;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;


public class HttpRequestModel {

	private Object requestpayload;
	private String endpoint;
	private HttpMethod method;
	private Map<String, String> uriVariables = new HashMap<>();
	
	public Object getRequestpayload() {
		return requestpayload;
	}
	public void setRequestpayload(Object requestpayload) {
		this.requestpayload = requestpayload;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public HttpMethod getMethod() {
		return method;
	}
	public void setMethod(HttpMethod method) {
		this.method = method;
	}
	public Map<String, String> getUriVariables() {
		return uriVariables;
	}
	public void setUriVariables(String name,String value) {
		this.uriVariables.put(name, value);
	}
}
