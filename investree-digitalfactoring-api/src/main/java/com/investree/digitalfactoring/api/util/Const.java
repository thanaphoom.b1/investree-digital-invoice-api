package com.investree.digitalfactoring.api.util;

public class Const {
	
	public static final String AWS_LAMBDA_RUNTIME = "AWS_LAMBDA";
	public static final String APP_NAME = "app.name";
	
	public static final String API_HEADER_CLIENT_ID_NAME = "api.header.ibm.clientid.key.name";
	public static final String API_HEADER_CLIENT_ID_VALUE = "api.header.ibm.clientid.key.value";
	public static final String API_HEADER_CLIENT_SECRET_NAME = "api.header.ibm.clientsecret.key.name";
	public static final String API_HEADER_CLIENT_SECRET_VALUE = "api.header.ibm.clientsecret.key.value";
	
	public static final String API_HEADER_X_API_KEY_NAME = "api.header.key.name";
	public static final String API_HEADER_X_API_KEY_VALUE = "api.header.key.value";
	
	public static final String BOT_API_INVOICE = "bot.api.invoice.endpoint";
	public static final String BOT_API_INVOICE_BATCH = "bot.api.invoice.batch.endpoint";
	public static final String BOT_API_INVOICE_STATUS = "bot.api.invoice.status.endpoint";
	public static final String BOT_API_INVOICE_ADJUST = "bot.api.invoice.adjust.endpoint";
	public static final String BOT_API_SELLER_DOUBLE_FINANCE = "bot.api.seller.endpoint";
	public static final String BOT_API_CONTRACT = "bot.api.contract.endpoint";
	public static final String CLIENT_STORE_FILE = "client.keystore.file";
	public static final String CLIENT_STORE_PASS="client.keystore.pass";
	
	public static final String BOT_API_HOST="BOT_API_HOST";
	
	
	public static interface API_HEADER{
		final String CLIENT_ID_NAME = "X-IBM-Client-Id";
		final String CLIENT_ID_VALUE = "IBM_CLIENT_ID_KEY_VALUE";
		final String CLIENT_SECRET_NAME = "X-IBM-Client-Secret";
		final String CLIENT_SECRET_VALUE = "IBM_CLIENT_SECRET_KEY_VALUE";
	}
	
	
}
