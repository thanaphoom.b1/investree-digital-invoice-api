package com.investree.digitalfactoring.api.service;

public interface ContractService {
	
	Object getContract(String sellerID,String buyerID,String poRef);
}
