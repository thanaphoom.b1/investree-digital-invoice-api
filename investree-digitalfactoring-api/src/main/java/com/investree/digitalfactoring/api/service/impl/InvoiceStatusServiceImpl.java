package com.investree.digitalfactoring.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.investree.digitalfactoring.api.entity.http.HttpRequestModel;
import com.investree.digitalfactoring.api.service.InvoiceStatusService;
import com.investree.digitalfactoring.api.util.Const;
import com.investree.digitalfactoring.api.util.RestAPICaller;

@Service
public class InvoiceStatusServiceImpl implements InvoiceStatusService {

	private static final Logger log = LoggerFactory.getLogger(InvoiceStatusService.class);
	
	@Autowired
	RestAPICaller api;
	
	@Autowired
	Environment env;
	
	@Override
	public Object setInvoiceStatus(Object payload) {
		HttpRequestModel request = new HttpRequestModel();
		request.setEndpoint(env.getProperty(Const.BOT_API_INVOICE_STATUS));
		request.setMethod(HttpMethod.POST);
		request.setRequestpayload(payload);
		return api.exchange(request);
	}

	@Override
	public Object getInvoiceStatus(String invoiceID, String sellerID, String buyerID, String issueDate) {
		HttpRequestModel request = new HttpRequestModel();
		log.debug("invoiceID: "+invoiceID);
		log.debug("sellerID: "+sellerID);
		log.debug("buyerID: "+buyerID);
		log.debug("issueDate: "+issueDate);
		
		request.setEndpoint(env.getProperty(Const.BOT_API_INVOICE_STATUS));
		request.setMethod(HttpMethod.GET);
		request.setUriVariables("invoiceID", invoiceID);
		request.setUriVariables("sellerID", sellerID);
		request.setUriVariables("buyerID", buyerID);
		request.setUriVariables("issueDate", issueDate);
		
		return api.exchange(request);
	}

}
