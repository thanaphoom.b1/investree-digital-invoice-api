package com.investree.digitalfactoring.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.investree.digitalfactoring.api.entity.http.HttpRequestModel;
import com.investree.digitalfactoring.api.entity.invoice.InvoiceRequestPayload;
import com.investree.digitalfactoring.api.service.InvoiceBatchService;
import com.investree.digitalfactoring.api.util.Const;
import com.investree.digitalfactoring.api.util.RestAPICaller;

@Service
public class InvoiceBatchImpl implements InvoiceBatchService {

	@Autowired
	RestAPICaller api;
	
	@Autowired
	Environment env;
	
	@Override
	public Object invoiceBatch(Object payload) {
		HttpRequestModel request = new HttpRequestModel();
		request.setEndpoint(env.getProperty(Const.BOT_API_INVOICE_BATCH));
		request.setMethod(HttpMethod.POST);
		request.setRequestpayload(payload);
		return api.exchange(request);
	}

}
