package com.investree.digitalfactoring.api.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class Configuration {
	
	@Autowired
	Environment env;
	
	private String runtime = System.getenv("RUNTIME");
	
	public  String getProperty(String name) {
		String value = "";
		if(Const.AWS_LAMBDA_RUNTIME.equals(runtime)) {
			value = System.getenv(name);
		}else {
			value = env.getProperty(name);
		}
		return value;
	}

}
