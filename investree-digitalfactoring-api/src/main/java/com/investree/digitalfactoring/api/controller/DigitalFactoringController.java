package com.investree.digitalfactoring.api.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.investree.digitalfactoring.api.service.ContractService;
import com.investree.digitalfactoring.api.service.InvoiceAdjustService;
import com.investree.digitalfactoring.api.service.InvoiceBatchService;
import com.investree.digitalfactoring.api.service.InvoiceService;
import com.investree.digitalfactoring.api.service.InvoiceStatusService;
import com.investree.digitalfactoring.api.service.SellerDoubleFinanceService;
import com.investree.digitalfactoring.api.util.Const;

@EnableWebMvc
@RestController
@CrossOrigin
@RequestMapping("/")
public class DigitalFactoringController {

	private static final Logger log = LoggerFactory.getLogger(DigitalFactoringController.class);

	@Autowired
	Environment env;
	@Autowired
	InvoiceService invoiceService;
	@Autowired
	InvoiceBatchService invoiceBatch;
	@Autowired
	InvoiceStatusService invoiceStatusService;
	@Autowired
	InvoiceAdjustService invoiceAdjustService;
	@Autowired
	SellerDoubleFinanceService sellerDoubleFinanceService;
	@Autowired
	ContractService contractService;

	@GetMapping(path = "/check")
	public String check() {

		String response = env.getProperty(Const.APP_NAME) + " is active. ";
		log.info(response);

		return response;
	}

	@GetMapping(path = "/invoice")
	public Object getInvoice(@RequestParam Map<String, String> allRequestParams) {
		log.info("getInvoice");
		String invoiceID = allRequestParams.get("invoiceID");
		String sellerID = allRequestParams.get("sellerID");
		String buyerID = allRequestParams.get("buyerID");
		String issueDate = allRequestParams.get("issueDate");

		return invoiceService.getInvoice(invoiceID, sellerID, buyerID, issueDate);
	}

	@PostMapping(path = "/invoice")
	public Object setInvoice(@RequestBody Object payload) {
		log.info("setInvoice");

		return invoiceService.setInvoice(payload);
	}

	@PostMapping(path = "/invoiceBatch")
	public Object getInvoiceBatch(@RequestBody Object data) {
		log.info("getInvoiceBatch");

		return invoiceBatch.invoiceBatch(data);
	}

	@RequestMapping(value = "invoiceStatus", method = RequestMethod.GET)
	public Object getInvoiceStatus(@RequestParam Map<String, String> allRequestParams) {
		log.info("getInvoiceStatus");
		
		//String status = allRequestParams.get("status");
		String invoiceID = allRequestParams.get("invoiceID");
		String sellerID = allRequestParams.get("sellerID");
		String buyerID = allRequestParams.get("buyerID");
		String issueDate = allRequestParams.get("issueDate");

		return invoiceStatusService.getInvoiceStatus(invoiceID, sellerID, buyerID, issueDate);
	}

	@PostMapping(path = "/invoiceStatus")
	public Object setInvoiceStatus(@RequestBody Object payload) {
		log.info("setInvoiceStatus");

		return invoiceStatusService.setInvoiceStatus(payload);
	}

	@PostMapping(path = "/invoiceAdjust")
	public Object setInvoiceAdjust(@RequestBody Object payload) {
		log.info("setInvoiceAdjust");

		return invoiceAdjustService.setInvoiceAdjust(payload);
	}

	@GetMapping(path = "/sellerDoubleFinance")
	public Object getSellerDoubleFinance(@RequestParam Map<String, String> allRequestParams) {
		log.info("getSellerDoubleFinance");
		String sellerID = allRequestParams.get("sellerID");

		return sellerDoubleFinanceService.getSellerDoubleFinance(sellerID);
	}

	@GetMapping(path = "/contract")
	public Object getContract(@RequestParam Map<String, String> allRequestParams) {
		log.info("getContract");
		String sellerID = allRequestParams.get("sellerID");
		String buyerID = allRequestParams.get("buyerID");
		String poRef = allRequestParams.get("poRef");

		return contractService.getContract(sellerID, buyerID, poRef);
	}
}
