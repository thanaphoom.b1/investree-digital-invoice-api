package com.investree.digitalfactoring.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.investree.digitalfactoring.api.entity.http.HttpRequestModel;
import com.investree.digitalfactoring.api.service.SellerDoubleFinanceService;
import com.investree.digitalfactoring.api.util.Const;
import com.investree.digitalfactoring.api.util.RestAPICaller;


@Service
public class SellerDoubleFinanceServiceImpl implements SellerDoubleFinanceService {

	
	@Autowired
	RestAPICaller api;
	
	@Autowired
	Environment env;
	
	@Override
	public Object getSellerDoubleFinance(String sellerID) {
		HttpRequestModel request = new HttpRequestModel();
		request.setEndpoint(env.getProperty(Const.BOT_API_SELLER_DOUBLE_FINANCE));
		request.setMethod(HttpMethod.GET);
		request.setUriVariables("sellerID", sellerID);
		
		return api.exchange(request);
	}

}
