package com.investree.digitalfactoring.api.service;

public interface SellerDoubleFinanceService {

	Object getSellerDoubleFinance(String sellerID);
}
