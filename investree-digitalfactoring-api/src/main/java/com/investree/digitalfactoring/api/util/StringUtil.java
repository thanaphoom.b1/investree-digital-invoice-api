package com.investree.digitalfactoring.api.util;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


import com.google.gson.Gson;

import lombok.Data;

@Data
public class StringUtil {

	private static String hexits = "0123456789abcdef";
	private int startIndex;
	private int endIndex;
	private int startTempInit;
	private int sumLength;
	
	public StringUtil(int startz , int sumLengthz) throws Exception{
		this.startIndex = startz;
		this.endIndex = 0;
		this.startTempInit = startz;
		this.sumLength = sumLengthz;
	}

	private static char cp838cp874[] = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			'\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', '\377', '\241', '\242', '\243', '\244', '\245', '\246', '\247', '[',
			'\374', '.', '<', '(', '+', '|', '&', '\240', '\250', '\251', '\252', '\253', '\254', '\255', '\256', ']',
			'!', '$', '*', ')', ';', '\375', '-', '/', '\257', '\260', '\261', '\262', '\263', '\264', '\265', '^',
			'\376', ',', '%', '_', '>', '?', '\337', '\356', '\266', '\267', '\270', '\271', '\272', '\273', '\274',
			'`', ':', '#', '@', '\'', '=', '"', '\357', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', '\275', '\276',
			'\277', '\300', '\301', '\302', '\372', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', '\303', '\304', '\305',
			'\306', '\307', '\310', '\373', '~', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\311', '\312', '\313', '\314',
			'\315', '\316', '\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367', '\370', '\371', '\317',
			'\320', '\321', '\322', '\323', '\324', '{', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', '\333', '\325',
			'\326', '\327', '\330', '\331', '}', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', '\332', '\340', '\341',
			'\342', '\343', '\344', '\\', '\334', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '\345', '\346', '\347',
			'\350', '\351', '\352', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '\353', '\354', '\355', '\335',
			'\336', ' ' };


	public static String toHexString(byte bytes[]) {
		StringBuffer retString = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; ++i) {
			String hex = Integer.toHexString(0x0100 + (bytes[i] & 0x00FF)).substring(1);
			if (hex.length() < 2) {
				retString.append("0");
			}
			retString.append(hex);
		}
		return retString.toString();
	}

	public static String asHex(byte buf[]) {
		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		for (int i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10)
				strbuf.append("0");
			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}
		return strbuf.toString();
	}

	public static String toUnicode(String src) {
		String result = "";
		if (src != null) {
			for (int i = 0; i < src.length(); i++)
				if ((int) src.charAt(i) > 0xA0 && (int) src.charAt(i) < 0x0E00)
					result += (char) ((((int) src.charAt(i)) - 0xA0) | 0x0E00);
				else
					result += src.charAt(i);
		}
		return result;
	}

	public static String toAscii(String src) {
		String result = "";
		if (src != null) {
			for (int i = 0; i < src.length(); i++) {
				if (Character.UnicodeBlock.of(src.charAt(i)) == Character.UnicodeBlock.THAI) {
					result += (char) ((int) (src.charAt(i) & 0x00FF) + 0xA0);
				} else {
					result += src.charAt(i);
				}
			}
		}
		return result;
	}

	public static String toAsciiTH(String strIn) {
		char instr[] = strIn.toCharArray();
		char outstr[] = new char[instr.length];

		for (int i = 0; i < instr.length; i++) {
			outstr[i] = cp838cp874[instr[i]];

		}

		String strOut = new String(outstr);
		return strOut;
	}

	/* CONCAT BYTE */
	public static byte[] concatenate(byte[] a, byte[] b) {
		byte[] r = new byte[a.length + b.length];
		System.arraycopy(a, 0, r, 0, a.length);
		System.arraycopy(b, 0, r, a.length, b.length);
		return r;
	}


	


	/* ASCII->UNICODE */
	public static String ASCII2Unicode(String ascii) {
		StringBuffer unicode = new StringBuffer(ascii);
		int code;
		for (int i = 0; i < ascii.length(); i++) {
			code = (int) ascii.charAt(i);
			if ((0xA1 <= code) && (code <= 0xFB)) {
				unicode.setCharAt(i, (char) (code + 0xD60));
			}
		}
		return unicode.toString();
	}

	/* UNICODE->ASCII */
	public static String Unicode2ASCII(String unicode) {
		StringBuffer ascii = new StringBuffer(unicode);
		int code;
		for (int i = 0; i < unicode.length(); i++) {
			code = (int) unicode.charAt(i);
			if ((0xE01 <= code) && (code <= 0xE5B)) {
				ascii.setCharAt(i, (char) (code - 0xD60));
			}
		}
		return ascii.toString();
	}

	

	public static String convertToDecimalPlaceString(String value) throws Exception {

		if (value == null || value.length() < 3) {

			return value;

		}

		String str = value.trim();
		BigDecimal dec = null;
		String decstr = "";
		DecimalFormat df = new DecimalFormat("0.00");
		if (str != null && str.length() > 0) {
			str = str.substring(0, str.length() - 2) + "." + str.substring(str.length() - 2, str.length());
			dec = new BigDecimal(str);
			decstr = df.format(dec);
		} else {
			decstr = str;
		}

		return decstr;

	}

	public static String getAmount(String input) throws Exception {
		String value = "";
		String prc = "";
		String[] chars = { "}", "J", "K", "L", "M", "N", "O", "P", "Q", "R" };
		String[] num = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		for (int i = 0; i < chars.length; i++) {
			if (input.contains(chars[i])) {
				prc = "-" + input.substring(0, input.length() - 1) + num[i];
				break;
			} else {
				prc = input;
			}
		}
		String decStr = prc.substring(0, prc.length() - 2) + "." + prc.substring(prc.length() - 2, prc.length());
		BigDecimal dec = new BigDecimal(decStr);
		value = dec.toString();
		return value;
	}

	public static String toHex(byte[] block) {
		StringBuffer buf = new StringBuffer();

		for (int i = 0; i < block.length; ++i) {
			buf.append(hexits.charAt((block[i] >>> 4) & 0xf));
			buf.append(hexits.charAt(block[i] & 0xf));
		}

		return buf + "";
	}

	public static String checkNullPacked(String data) {
		String value = "0";
		if (data.trim().length() <= 0) {
			value = "0";
		} else {
			value = data.trim();
		}
		return value;
	}

	private static String getAbsoluteAmount(String amount) {
		String value = "";
		if (amount.contains("-")) {
			value = amount.substring(amount.indexOf("-"), amount.length());
		} else {
			value = amount;
		}
		return value;
	}

	/**
	 * input 123456 return 1234.56 (BigDecimal)
	 */
	public static BigDecimal convertToDecimalPlace(String value)
			throws Exception {
		String str =getAbsoluteAmount(value.trim());
		BigDecimal dec = null;
		if (str != null && str.length() > 0) {
			dec = new BigDecimal(str);
		} else {
			dec = new BigDecimal("0");
			//throw new Exception("invalid value to convert. " + str);
		}

		return dec;
	}

	public static String resolveAsPath(String absolutPath) {
		String path = "";
		path = absolutPath.replace("\\", "/");
		return path;
	}
	
	public static String getValueStr(String data) {

		try {
			
			return (data == null ? "" : data);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";

	}
	
	public static boolean isNullOrEmpty(Object data) {

		try {
			if(data == null) {
				return true;
			}else if(String.valueOf(data).trim().length()==0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}
	
	public static String getJSONStr(Object obj){
		Gson json = new Gson();
		String strJson = json.toJson(obj);
		return strJson;
	}
	
	public static <T> T getJSONStrToObject(Object obj,Class<T> clazz){
		Gson gson = new Gson();
		return gson.fromJson((String) obj, clazz);
	}
	
	
	
	public String cutWordByCustomLength(String data,String fieldName ,int lengthField )throws Exception{
		String value = "";
		int startz = getStartIndex();
		int endz =  (startz + lengthField);
		setEndIndex(endz);
		if(data !=  null && data.length() > 0){
			value = data.substring( startz , endz);
			//log.debug((fieldName+" ["+ (startz+1) +","+ endz +"]: "+value));
			int sum = incrementSum(lengthField);
			setSumLength(sum);//summary length word all
			setStartIndex(endz);//increment start index
		}
		return value;
	}
	
	public String cutByteByCustomLength(byte[] data,String fieldName ,int lengthField )throws Exception{
		byte[] byteValue = new byte[lengthField];
		String value = "";
		int startz = getStartIndex();
		int endz =  (startz + lengthField);
		setEndIndex(endz);
		if(data !=  null && data.length > 0){
			int j = 0;
			for(int i = startz; i < startz+lengthField; i++){
				byteValue[j] = data[i];
				j++;
			}
			int sum = incrementSum(lengthField);
			setSumLength(sum);//summary length word all
			setStartIndex(endz);//increment start index
			value = new String(byteValue);
			//log.debug((fieldName+" ["+ (startz+1) +","+ endz +"]: "+value));
		}
		return value;
	}
	
	public int incrementSum(int lengthz) {
		return sumLength + lengthz;
	}
	
	
	/**
	 * Return System Date ddMMyyyyHHmmssSSS
	 * **/
	public static String getCurrentSystemDateStr() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmssSSS");
		String systemDate = dateFormat.format(new Date());
		
		return systemDate;
	}
	

	public static void main(String[] args) {
		try {
			
			//validateTaxID("11");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// update by vipawadee 06/08/2019
	public static String MD5Checksum(String Checksum) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(Checksum.getBytes());
        
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
		return sb.toString();
		
	}

}
