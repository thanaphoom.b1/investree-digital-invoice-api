package com.investree.digitalfactoring.api.service;

public interface InvoiceStatusService {

	Object setInvoiceStatus(Object payload);

	Object getInvoiceStatus(String invoiceID, String sellerID, String buyerID, String issueDate);
}
