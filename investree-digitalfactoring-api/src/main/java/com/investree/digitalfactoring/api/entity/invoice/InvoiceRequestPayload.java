package com.investree.digitalfactoring.api.entity.invoice;

import lombok.Data;

@Data
public class InvoiceRequestPayload {
	
	
	private String invoiceID;
	
	
	private String sellerID;
	
	
	private String sellerBranch;
	
	
	private String platformID;
	
	
	private String PORef;
	
	
	private String buyerID;
	
	
	private String amount;
	
	
	private String dueDate;
	
	
	private String issueDate;
	
	
	private String isFinanced;

}
