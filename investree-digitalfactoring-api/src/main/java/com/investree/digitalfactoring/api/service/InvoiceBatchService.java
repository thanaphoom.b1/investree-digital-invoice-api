package com.investree.digitalfactoring.api.service;

import java.util.List;

import com.investree.digitalfactoring.api.entity.invoice.InvoiceRequestPayload;

public interface InvoiceBatchService {

	Object invoiceBatch(Object payload);
}
