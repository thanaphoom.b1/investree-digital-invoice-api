package com.investree.digitalfactoring.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.investree.digitalfactoring.api.entity.http.HttpRequestModel;
import com.investree.digitalfactoring.api.service.ContractService;
import com.investree.digitalfactoring.api.util.Const;
import com.investree.digitalfactoring.api.util.RestAPICaller;

@Service
public class ContractServiceImpl implements ContractService {

	@Autowired
	RestAPICaller api;
	
	@Autowired
	Environment env;
	
	@Override
	public Object getContract(String sellerID, String buyerID, String poRef) {
		HttpRequestModel request = new HttpRequestModel();
		request.setEndpoint(env.getProperty(Const.BOT_API_CONTRACT));
		request.setMethod(HttpMethod.GET);
		request.setUriVariables("sellerID", sellerID);
		request.setUriVariables("buyerID", buyerID);
		request.setUriVariables("PORef", poRef);
		
		return api.exchange(request);
	}

}
