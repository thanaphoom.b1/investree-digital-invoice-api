package com.investree.digitalfactoring.api.service;

import com.investree.digitalfactoring.api.entity.invoice.InvoiceRequestPayload;

public interface InvoiceService {

	Object getInvoice(String invoiceID, String sellerID, String buyerID, String issueDate);

	Object setInvoice(Object payload);
}
