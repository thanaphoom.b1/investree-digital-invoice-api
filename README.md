# Getting Started Investree Digital Factoring APIs

This project created by Springboot with maven

## Prerequisite
- Java 8 Runtime
- Maven 3
- Springboot 2.3
- AWS Lambda

## Configuration [localhost]
/src/main/resources/application.properties
- Define Server port

server.port=<port>


## Start Application [localhost]

Run main class InvestreeDigitalfactoringApiApplication.java


## Start Application AWS Lambda

Lambda function Handler: com.investree.api.lambda.AWSLambdaHandler

### Configuration Environment variable
- RUNTIME = AWS_LAMBDA
- BOT_API_HOST = BOT host
- IBM_CLIENT_ID_KEY_VALUE = Client ID
- IBM_CLIENT_SECRET_KEY_VALUE = Secret ID